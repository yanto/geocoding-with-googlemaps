var map = null;
var markers = [];
var pathLine = null;
var paths = [];
var distanceLabels = [];
// NOTE: Store markers, path lines and distance labels in arrays so that they can be cleared when a new result is rendered.

function initMap() {
    var tokyo = new google.maps.LatLng(35.6733227, 139.6403488);
    var mapOptions = {
        center: tokyo,
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
}

function submit_form() {
    $('#locations').submit(function(e) {
        var targetUrl = "/ajax";
        $.ajax({
            type: "POST",
            url: targetUrl,
            data: $('#locations').serialize(),
            success: function(data) {
                display(data);
            }
        });
        // Block the traditional submission of the form.
        e.preventDefault();
    });

    // Inject CSRF token into AJAX request.
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", "{{ csrf_token }}")
            }
        }
    });
}

function display(data) {
    clearMap();

    var bounds = new google.maps.LatLngBounds();
    $.each(data.locations, function(index, location) {
        var position = new google.maps.LatLng(location.position.lat, location.position.lng);
        bounds.extend(position);

        var marker = new MarkerWithLabel({
            map: map,
            animation: google.maps.Animation.DROP,
            position: position,
            labelContent: location.geohash,
            labelAnchor: new google.maps.Point(32, 0),
            labelClass: "marker-label", // your desired CSS class
            labelInBackground: true,
            labelStyle: {opacity: 0.75}
        });
        markers.push(marker);

        paths.push({lat: location.position.lat, lng: location.position.lng});
    });

    paths.push({lat: data.locations[0].position.lat, lng: data.locations[0].position.lng});

    pathLine = new google.maps.Polyline({
        map: map,
        path: paths,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 3
    });

    $.each(data.distances, function(index, distance) {
        var position = new google.maps.LatLng(distance.position.lat, distance.position.lng);
        var distanceLabel = new MapLabel({
            map: map,
            position: position,
            text: distance.distance,
            fontSize: 15,
            fontColor: '#0000FF',
            strokeWeight: 6
        });
        distanceLabels.push(distanceLabel)
    });

    // Adjust zoom level so that all 3 locations are visible on the map.
    map.fitBounds(bounds);
    map.panToBounds(bounds);
}


function clearMap() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    markers = [];

    if (pathLine != null) {
        pathLine.setMap(null);
    }
    paths = []

    for (var i = 0; i < distanceLabels.length; i++) {
        distanceLabels[i].setMap(null);
    }
    distanceLabels = [];
}
