import math


def midpoint(lat1, lng1, lat2, lng2):
    # Convert coordinates from degrees to radians
    lat_r1 = math.radians(lat1)
    lng_r1 = math.radians(lng1)
    lat_r2 = math.radians(lat2)
    lng_r2 = math.radians(lng2)

    bx = math.cos(lat_r2) * math.cos(lng_r2 - lng_r1)
    by = math.cos(lat_r2) * math.sin(lng_r2 - lng_r1)
    lat_r3 = math.atan2(math.sin(lat_r1) + math.sin(lat_r2),
                        math.sqrt((math.cos(lat_r1) + bx) * (math.cos(lat_r1) + bx) + by**2))
    lng_r3 = lng_r1 + math.atan2(by, math.cos(lat_r1) + bx)

    return {'lat': round(math.degrees(lat_r3), 7), 'lng': round(math.degrees(lng_r3), 7)}
