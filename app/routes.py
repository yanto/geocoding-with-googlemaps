from flask import jsonify, render_template
from flask_wtf import FlaskForm
from wtforms import StringField

import googlemaps
import itertools
import pygeohash as pgh

from app import app, utils


class MapsForm(FlaskForm):
    loc01 = StringField('loc01', default='東京タワー')
    loc02 = StringField('loc02', default='みなとみらい')
    loc03 = StringField('loc03', default='井の頭公園')


@app.route('/')
def index():
    form = MapsForm()
    return render_template('index.html', form=form)


@app.route('/ajax', methods=['POST'])
def process_form():
    form = MapsForm()
    if form.validate_on_submit():
        gmaps = googlemaps.Client(key='AIzaSyAaw_QhupG4EKMT034XUZ0f--5Y-Pzx_SY')
        geohashes = []
        locations = []
        distances = []

        for fieldname, value in form.data.items():
            if fieldname.startswith('loc'):
                geocode = gmaps.geocode(value)
                position = geocode[0].get('geometry').get('location')
                geohash = pgh.encode(position.get('lat'), position.get('lng'), precision=7)

                geohashes.append(geohash)
                locations.append({
                    'location': value,
                    'position': position,
                    'geohash': geohash
                })

        for origin, destination in itertools.combinations(geohashes, 2):
            lat_origin, lng_origin = pgh.decode(origin)
            lat_destination, lng_destination = pgh.decode(destination)
            position = utils.midpoint(lat_origin, lng_origin, lat_destination, lng_destination)
            distance = pgh.geohash_haversine_distance(origin, destination)

            distances.append({
                'position': position,
                'distance': '{0:.2f} km'.format(distance/1000)
            })

        return jsonify({
            'locations': locations,
            'distances': distances
        })

    return jsonify(data=form.errors)
