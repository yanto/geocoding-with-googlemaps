## Problem Description

Pick up 3 favorite locations in address character. Geocode these addresses to latitude and longitude respectively, encode these latitude and longitude to geohash 7 characters, and use them as labels/attribute on your map app.

You can choose any mapping apps on your own such as Google Maps or QGIS.

Bonus if you code to calculate distance in meter among 3 of locations respectively.

Submit:

- Address characters of 3 locations you chose.
- Code for geocoding and geohashing, and the returned values for mapping apps.
- Description of library, API set, and build environment you used.


## Solution Description

The solution is presented as a simple web application. The backend is written in Python using Flask micro framework, and the frontend is a simple HTML page containing a web form to enter the three locations, and a Google map to display the result.

The backend uses the Python client for Google Maps services, and the frontend uses the Google Maps JavaScript API to access the Google Maps APIs. 

Submitting the web form will create an AJAX request to the backend, which will use the Google Maps API to obtain the geographical coordinates. The coordinates are then passed to another library to generate the geohashes and calculate the distances. The result is then returned in JSON format.


## Directory structure

        geocoding-with-googlemaps
        |
        -- app
        |   |
        |   -- __init__.py
        |   -- routes.py
        |   -- utils.py
        |   |
        |   -- static
        |   |   |
        |   |   -- maps.js
        |   |   -- style.css
        |   |
        |   -- templates
        |       |
        |       -- index.html
        |
        -- config.py
        -- requirements.txt

Description for each file:

* **__init__.py**: Creates the application object as an instance of class Flask. Flask also uses the location of the module passed here as a starting point when it needs to load resources.
* **routes.py**: Maps view functions to one or more route URLs so that Flask knows what logic to execute when a client requests a given URL.
* **utils.py**: Helper functions. In this case, there is only one function to calculate the midpoint between two geographical coordinates.
* **maps.js**: Javascript functions to initialise Google map, handle form submissions and rendering result returned by the backend on the map.
* **style.css**: Stylesheet for customising look and feel of web form and labels on the map.
* **index.html**: HTML page containing a web form to enter the three locations, and a Google map to display the result.
* **config.py**: Stores application settings. For this application, it stores SECRET_KEY that is used for generating CSRF token for the web form.


## Dependencies

Python (requirements.txt):

* [flask](flask.pocoo.org/): Python micro web framework
* [flask-wtf](https://flask-wtf.readthedocs.io/): Flask integration with WTForms for handling web forms
* [googlemaps](https://github.com/googlemaps/google-maps-services-python): Python client for Google Maps services
* [pygeohash](https://pypi.org/project/pygeohash/): Python geohashing library

Javascript:

* [Google Maps JavaScript API](https://developers.google.com/maps/documentation/javascript/tutorial)
* [Google Maps V3 Utility Library](https://github.com/googlemaps/v3-utility-library). For this application, MapLabel and MarkerWithLabel are used for displaying geohashes and distances between two locations.
* [jQuery](https://jquery.com/) for JavaScript convenient functions.


## Address characters of 3 locations and their returned values from libraries

NOTE: They are already predefined in MapsForm class inside **routes.py**

* 東京タワー

        Google Maps API:
            [{'address_components': [{'long_name': '8', 'short_name': '8', 'types': ['premise']}, {'long_name': '2', 'short_name': '2', 'types': ['political', 'sublocality', 'sublocality_level_4']}, {'long_name': '4 Chome', 'short_name': '4 Chome', 'types': ['political', 'sublocality', 'sublocality_level_3']}, {'long_name': 'Shibakoen', 'short_name': 'Shibakoen', 'types': ['political', 'sublocality', 'sublocality_level_2']}, {'long_name': 'Minato', 'short_name': 'Minato', 'types': ['locality', 'political']}, {'long_name': 'Tokyo', 'short_name': 'Tokyo', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'Japan', 'short_name': 'JP', 'types': ['country', 'political']}, {'long_name': '105-0011', 'short_name': '105-0011', 'types': ['postal_code']}], 'formatted_address': '4 Chome-2-8 Shibakoen, Minato, Tokyo 105-0011, Japan', 'geometry': {'location': {'lat': 35.6585805, 'lng': 139.7454329}, 'location_type': 'ROOFTOP', 'viewport': {'northeast': {'lat': 35.6599294802915, 'lng': 139.7467818802915}, 'southwest': {'lat': 35.6572315197085, 'lng': 139.7440839197085}}}, 'place_id': 'ChIJCewJkL2LGGAR3Qmk0vCTGkg', 'types': ['establishment', 'point_of_interest', 'premise']}]

        pygeohash:
            xn76ggr

* みなとみらい

        Google Maps API:
            [{'address_components': [{'long_name': 'Minatomirai', 'short_name': 'Minatomirai', 'types': ['political', 'sublocality', 'sublocality_level_2']}, {'long_name': 'Nishi Ward', 'short_name': 'Nishi Ward', 'types': ['political', 'sublocality', 'sublocality_level_1']}, {'long_name': 'Yokohama', 'short_name': 'Yokohama', 'types': ['locality', 'political']}, {'long_name': 'Kanagawa Prefecture', 'short_name': 'Kanagawa Prefecture', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'Japan', 'short_name': 'JP', 'types': ['country', 'political']}, {'long_name': '220-0012', 'short_name': '220-0012', 'types': ['postal_code']}], 'formatted_address': 'Minatomirai, Nishi Ward, Yokohama, Kanagawa Prefecture 220-0012, Japan', 'geometry': {'bounds': {'northeast': {'lat': 35.46691140000001, 'lng': 139.6393995}, 'southwest': {'lat': 35.4524222, 'lng': 139.6246394}}, 'location': {'lat': 35.4622861, 'lng': 139.632353}, 'location_type': 'APPROXIMATE', 'viewport': {'northeast': {'lat': 35.46691140000001, 'lng': 139.6393995}, 'southwest': {'lat': 35.4524222, 'lng': 139.6246394}}}, 'place_id': 'ChIJ_-Fx-UJcGGARRLCtp76VLnA', 'types': ['political', 'sublocality', 'sublocality_level_2']}]

        pygeohash:
            xn739rt

* 井の頭公園

        Google Maps API:
            [{'address_components': [{'long_name': 'Inokashira Park', 'short_name': 'Inokashira Park', 'types': ['establishment', 'park', 'point_of_interest']}, {'long_name': '31', 'short_name': '31', 'types': ['premise']}, {'long_name': '18', 'short_name': '18', 'types': ['political', 'sublocality', 'sublocality_level_4']}, {'long_name': '1 Chome', 'short_name': '1 Chome', 'types': ['political', 'sublocality', 'sublocality_level_3']}, {'long_name': 'Gotenyama', 'short_name': 'Gotenyama', 'types': ['political', 'sublocality', 'sublocality_level_2']}, {'long_name': 'Musashino', 'short_name': 'Musashino', 'types': ['locality', 'political']}, {'long_name': 'Tokyo', 'short_name': 'Tokyo', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'Japan', 'short_name': 'JP', 'types': ['country', 'political']}, {'long_name': '180-0005', 'short_name': '180-0005', 'types': ['postal_code']}], 'formatted_address': 'Inokashira Park, 1 Chome-18-31 Gotenyama, Musashino, Tokyo 180-0005, Japan', 'geometry': {'location': {'lat': 35.6997435, 'lng': 139.5731906}, 'location_type': 'ROOFTOP', 'viewport': {'northeast': {'lat': 35.70109248029149, 'lng': 139.5745395802915}, 'southwest': {'lat': 35.6983945197085, 'lng': 139.5718416197085}}}, 'place_id': 'ChIJLWaVdDXuGGART_Pg1R3CZ4A', 'types': ['establishment', 'park', 'point_of_interest']}]

        pygeohash:
            xn7704f


## Distance calculation using pygeohash

    東京タワー to みなとみらい (in metres): 24746.755515638895
    東京タワー to 井の頭公園 (in metres): 16855.423589624846
    みなとみらい to 井の頭公園 (in metres): 27232.829098140828


## How to run the web application

00. Prerequisite: The web application is tested on Python 3.6, but any Python version 3.x should work.

01. From the root directory of solution01, create a Python virtual environment and activate.

        $ python3 -m venv venv
        $ source venv/bin/activate

02. Install pip, setuptools and wheel.

        (venv)$ pip install --upgrade pip setuptools wheel

03. Install the dependencies.

        (venv)$ pip install -r requirements.txt

04. Start the application.

        (venv)$ export FLASK_APP=app/__init__.py; flask run --host=0.0.0.0

05. If everything works correctly, you should be able to view the HTML page by opening http://localhost:5000 on a web browser. Enter the three locations, submit the form and the result will be displayed on the Google map. A screenshot of a successful run can be found at **screenshot.png**.
